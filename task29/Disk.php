<?php

namespace Task29;

use Task29\FigureInterface as FigureInterface;
use Task29\CircleInterface as CircleInterface;

/**
 * Class Disk
 *
 * @package Task29
 *
 * @param float $r
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Disk implements FigureInterface, CircleInterface
{
    private $r;

    /**
     * Disk constructor.
     *
     * @param float $r
     */
    public function __construct(float $r)
    {
        $this->r = $r;
    }

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float
    {
        return 2 * $this->r * pi();
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        return pow($this->r, 2) * pi();
    }

    /**
     * get Radius
     *
     * @return float
     */
    public function getRadius(): float
    {
        return $this->r;
    }

    /**
     * get Diameter
     *
     * @return float
     */
    public function getDiameter(): float
    {
        return 2 * $this->r;
    }
}