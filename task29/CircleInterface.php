<?php

namespace Task29;

/**
 * Interface CircleInterface
 *
 * @package Task29
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface CircleInterface
{
    /**
     * get Radius
     *
     * @return float
     */
    public function getRadius(): float ;

    /**
     * get Diameter
     *
     * @return float
     */
    public function getDiameter(): float ;
}