<?php

namespace Task29;

/**
 * Interface TetragonInterface
 *
 * @package Task29
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface TetragonInterface
{
    /**
     * get A
     *
     * @return float
     */
    public function getA(): float ;

    /**
     * get B
     *
     * @return float
     */
    public function getB(): float ;

    /**
     * get C
     *
     * @return float
     */
    public function getC(): float ;

    /**
     * get D
     *
     * @return float
     */
    public function getD(): float ;
}