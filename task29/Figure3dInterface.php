<?php

namespace Task29;

/**
 * Interface Figure3dInterface
 *
 * @package Task29
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface Figure3dInterface
{
    /**
     * get Volume
     *
     * @return float
     */
    public function getVolume(): float ;

    /**
     * get Surface Square
     *
     * @return float
     */
    public function getSurfaceSquare(): float ;
}