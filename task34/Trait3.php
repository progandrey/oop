<?php

namespace Task34;

/**
 * Trait Trait3
 *
 * @package Task34
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
trait Trait3
{
    /**
     * method
     *
     * @return int
     */
    private function method(): int
    {
        return 3;
    }
}