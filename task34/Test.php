<?php

namespace Task34;

use Task34\Trait1 as Trait1;
use Task34\Trait2 as Trait2;
use Task34\Trait3 as Trait3;

/**
 * Class Test
 *
 * @package Task34
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Test
{
    /**
     * add one and two and three;
     */
    use Trait1, Trait2, Trait3;

    /**
     * get Sum
     *
     * @return int
     */
    public function getSum(): int
    {
        return $this->method() + $this->method() + $this->method();
    }
}