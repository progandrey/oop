<?php
require_once 'vendor/autoload.php';

/**
 * Task 8
 */

use Task8\Student as Student8;

echo "<br><br>Задание 8<br>";
$obj8 = new Student8('Ivan', 4);
$obj8->transferToNextCourse();
$obj8->transferToNextCourse();

$obj8->setCourseAdministrator('Anton');
echo $obj8->getCourseAdministrator();

$obj8->courseAdministrator = 'Filip';