<?php

namespace Task1;

/**
 * Class Employee
 *
 * @package Task1
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 * @param float $salary
 */
class Employee
{
    public $name;
    public $age;
    public $salary;
}