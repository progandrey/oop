<?php

namespace Task27;

/**
 * Interface EmployeeInterface
 *
 * @package Task27
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface EmployeeInterface extends UserInterface
{
    /**
     * get Salary
     *
     * @return float
     */
    public function getSalary(): float ;

    /**
     * set Salary
     *
     * @param float $salary
     *
     * @return mixed
     */
    public function setSalary(float $salary);
}