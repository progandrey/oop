<?php

namespace Task27;

use Task27\EmployeeInterface as EmployeeInterface;

/**
 * Class Employee
 *
 * @package Task27
 *
 * @param string $name
 * @param int $age
 * @param float $salary
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Employee implements EmployeeInterface
{
    private $name;
    private $age;
    private $salary;

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * set Name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * set Age
     *
     * @param int $age
     */
    public function setAge(int $age)
    {
        $this->age = $age;
    }

    /**
     * get Salary
     *
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }

    /**
     * set Salary
     *
     * @param float $salary
     */
    public function setSalary(float $salary)
    {
        $this->salary = $salary;
    }
}