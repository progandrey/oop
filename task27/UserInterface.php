<?php

namespace Task27;

/**
 * Interface UserInterface
 *
 * @package Task27
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface UserInterface
{
    /**
     * UserInterface constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age);

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string ;

    /**
     * set Name
     *
     * @param string $name
     *
     * @return mixed
     */
    public function setName(string $name);

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int ;

    /**
     * set Age
     *
     * @param int $age
     *
     * @return mixed
     */
    public function setAge(int $age);
}