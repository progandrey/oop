<?php

namespace Task21;

/**
 * Class User
 *
 * @package Task21
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param string $surname
 * @param string $birthday
 * @param int $age
 */
class User
{
    private $name;
    private $surname;
    private $birthday;
    private $age;

    /**
     * User constructor.
     *
     * @param string $name
     * @param string $surname
     * @param string $birthday
     */
    public function __construct(string $name, string $surname, string $birthday)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * get Sur Name
     *
     * @return string
     */
    public function getSurName(): string
    {
        return $this->surname;
    }

    /**
     * get Birth Day
     *
     * @return string
     */
    public function getBirthDay(): string
    {
        return $this->birthday;
    }

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * calculate Age
     *
     * @param string $birthday
     *
     * @return float
     */
    protected function calculateAge(string $birthday)
    {
        return floor((time() - strtotime($birthday)) / 31556926);
    }
}