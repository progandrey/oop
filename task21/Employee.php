<?php

namespace Task21;

use Task21\User as User;

/**
 * Class Employee
 *
 * @package Task21
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param float $salary
 */
class Employee extends User
{
    private $salary;

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param string $surname
     * @param string $birthday
     * @param float $salary
     */
    public function __construct(string $name, string $surname, string $birthday, float $salary)
    {
        parent::__construct($name, $surname, $birthday);
        $this->salary = $salary;
    }

    /**
     * get Salary
     *
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}