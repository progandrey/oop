<?php

namespace Task40;

use Task40\DoorInterface as DoorInterface;

/**
 * Class Wardrobe
 *
 * @param string $door
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @package Task40
 *
 * @version 1.0.0
 */
class Wardrobe implements DoorInterface
{
    protected $door;

    /**
     * Wardrobe constructor.
     */
    public function __construct()
    {
        $this->closeDoor();
    }

    /**
     * open Door
     */
    public function openDoor()
    {
        $this->door = 'open';
    }

    /**
     * close Door
     */
    public function closeDoor()
    {
        $this->door = 'close';
    }
}