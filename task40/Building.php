<?php

namespace Task40;

/**
 * Class Building
 *
 * @param Wardrobe $wardrobe
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @package Task40
 *
 * @version 1.0.0
 */
abstract class Building
{
    protected $wardrobe;

    /**
     * Building constructor.
     */
    public function __construct()
    {
        $this->wardrobe = new Wardrobe();
    }
}