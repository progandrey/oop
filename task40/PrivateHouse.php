<?php

namespace Task40;

use Task40\Building as Building;
use Task40\DoorInterface as DoorInterface;
use Task40\ElectricityInterface as ElectricityInterface;

/**
 * Class PrivateHouse
 *
 * @param Fridge $fridge
 * @param Microwave $microwave
 * @param string $window
 * @param string $door
 * @param string $current
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @package Task40
 *
 * @version 1.0.0
 */
class PrivateHouse extends Building implements DoorInterface, ElectricityInterface
{
    private $fridge;
    private $microwave;
    private $window;
    private $door;
    private $current;

    /**
     * PrivateHouse constructor.
     *
     * @param Fridge $fridge
     * @param Microwave $microwave
     */
    public function __construct(Fridge $fridge, Microwave $microwave)
    {
        parent::__construct();
        $this->fridge = $fridge;
        $this->microwave = $microwave;
        $this->window = $this->openWindow();
        $this->door = $this->closeDoor();
        $this->current = $this->on();
    }

    /**
     * Open door
     */
    public function openDoor()
    {
        $this->door = 'open';
    }

    /**
     * Close door
     */
    public function closeDoor()
    {
        $this->door = 'close';
    }

    /**
     * Open window
     */
    public function openWindow()
    {
        $this->window = 'open';
    }

    /**
     * Close window
     */
    public function closeWindow()
    {
        $this->window = 'close';
    }

    /**
     * on current
     */
    public function on()
    {
        $this->current = 'on';
    }

    /**
     * off current
     */
    public function off()
    {
        $this->current = 'off';
    }
}