<?php

namespace Task40;

use Task40\DoorInterface as DoorInterface;
use Task40\ElectricityInterface as ElectricityInterface;

/**
 * Class Fridge
 *
 * @param string $door
 * @param string $current
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @package Task40
 *
 * @version 1.0.0
 */
class Fridge implements DoorInterface, ElectricityInterface
{
    protected $door;
    protected $current;

    public function __construct()
    {
        $this->door = $this->closeDoor();
        $this->current = $this->off();
    }

    /**
     * Open door
     */
    public function openDoor()
    {
        $this->door = 'open';
    }

    /**
     * Close door
     */
    public function closeDoor()
    {
        $this->door = 'close';
    }

    /**
     * on current
     */
    public function on()
    {
        $this->current = 'on';
    }

    /**
     * off current
     */
    public function off()
    {
        $this->current = 'off';
    }
}