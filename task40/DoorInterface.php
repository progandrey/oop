<?php

namespace Task40;

/**
 * Interface DoorInterface
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @package Task40
 *
 * @version 1.0.0
 */
interface DoorInterface
{
    public function openDoor();

    public function closeDoor();
}