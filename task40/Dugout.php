<?php

namespace Task40;

use Task40\Building as Building;
use Task40\DoorInterface as DoorInterface;

/**
 * Class Dugout
 *
 * @param string $door
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @package Task40
 *
 * @version 1.0.0
 */
class Dugout extends Building implements DoorInterface
{
    private $door;

    /**
     * Dugout constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->door = $this->closeDoor();
    }

    /**
     * Open door
     */
    public function openDoor()
    {
        $this->door = 'open';
    }

    /**
     * Close door
     */
    public function closeDoor()
    {
        $this->door = 'close';
    }
}