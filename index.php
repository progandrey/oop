<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'vendor/autoload.php';

$start = microtime(true);
/**
 * Task 24
 */

use Task24\User as User24;

$obj24 = new User24();

echo "<br><br><b>HOME WORK #4<br><br>Task 24</b><br>";
$obj24->setName('Andrey');
$obj24->setAge(25);
echo '<br>Name: ' . $obj24->getName();
echo '<br>Age: ' . $obj24->getAge();

/**
 * Task 25
 */

use Task25\Cube as Cube25;

$obj25 = new Cube25(3);
echo "<br><br><b>Task 25</b><br>";
echo '<br>Volume: ' . $obj25->getVolume();
echo '<br>Square: ' . $obj25->getSquare();

/**
 * Task 26
 */

use Task26\User as User26;

$obj26 = new User26('Andrey', 83);
echo "<br><br><b>Task 26</b><br>";
echo '<br>Name: ' . $obj26->getName();
echo '<br>Age: ' . $obj26->getAge();

/**
 * Task 27
 */

use Task27\Employee as Employee27;

$obj27 = new Employee27('Andrey', 83);
echo "<br><br><b>Task 27</b><br>";
$obj27->setSalary(400);
echo '<br>Name: ' . $obj27->getName();
echo '<br>Age: ' . $obj27->getAge();
echo '<br>Salary: ' . $obj27->getSalary();

/**
 * Task 28
 */

use Task28\Cube as Cube28;
use Task28\Figure3dInterface as Figure3dInterface;
use Task28\FigureInterface as FigureInterface;
use Task28\Rectangle as Rectangle28;
use Task28\Quadrate as Quadrate28;

// 28.4
$obj2811 = new Cube28(3);
$obj1812 = new Cube28(2);

$obj2821 = new Rectangle28(4, 7);
$obj2822 = new Rectangle28(8, 3);

$obj2831 = new Quadrate28(4, 7, 3, 6, 12, 43);
$obj2832 = new Quadrate28(2, 5, 7, 3, 12, 65);

$arr28 = array();
$arr28[] = $obj2811;
$arr28[] = $obj2832;
$arr28[] = $obj1812;
$arr28[] = $obj2831;
$arr28[] = $obj2822;
$arr28[] = $obj2821;

// 28.5
echo "<br><br><b>Task 28</b><br>";
echo "<br><br>FigureInterface:<br>";

foreach ($arr28 AS $value) {
    if ($value instanceof FigureInterface) {
        echo 'Square: ' . $value->getSquare() . '<br>';
    }
}

// 28.6
echo "<br>#28.6:<br>";
foreach ($arr28 AS $value) {
    if ($value instanceof FigureInterface) {
        echo 'Square: ' . $value->getSquare() . '<br>';
    }

    if ($value instanceof Figure3dInterface) {
        echo 'SurfaceSquare: ' . $value->getSurfaceSquare() . ' - Volume: ' . $value->getVolume() . '<br>';
    }
}

/**
 * Task 29 - 33
 */

/**
 * Task 34
 */
use App\BaseException as BaseException;
use App\FileLogException as FileLogException;
use App\DirectoryException as DirectoryException;
use App\FileSystemException as FileSystemException;
use App\FileException as FileException;
use App\InitFileNotFoundExeption as InitFileNotFoundExeption;

//use Task34\Test as Test34;

//$obj34 = new Test34();

echo "<br><br><b>Task 34</b><br>";
//echo $obj34->getSum();

/**
 * Task 35
 */

use Task35\Test as Test35;

$obj35 = new Test35();

echo "<br><br><b>Task 35</b><br>";
echo $obj35->getSum();

/**
 * Task 38
 */
echo "<br><br><b>Task 38</b><br>";

try {
    $init_file_path = 'init.php';
    if (!file_exists($init_file_path)) {
        throw new InitFileNotFoundExeption("Init file not found.");
    }
} catch (InitFileNotFoundExeption $e) {
    echo '38.1: ' . $e->getMessage();
    error_log("\n\tTask38: Exeption1: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
} catch (Error $e) {
    echo $e->getMessage();
}

//2
$message = 'Trace: ';

try {
    if (!isset($one)) {
        throw new FileLogException($message, 0, $e);
    }
    echo $one;
} catch (FileLogException $e) {
    echo '<br>38.2 ' . $e->getMessage() . $e->getTraceAsString();
    error_log("\n\tTask38: Exeption2: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
} catch (Error $e) {
    $e->getMessage();
}

//3
try {
    $b = array();
    if (!isset($b['a'])) {
        throw new BaseException("not found \$b['a']. <br><br>");
    }
    $b['a'];
} catch (BaseException $e) {
    echo "<br>38.3 Base Exception: " . $e->getMessage();
} catch (Error $e) {
    echo $e->getMessage();
}

/**
 * Task 41
 */
echo "<br><br><b>Task 41</b><br>";

function shutdown()
{
    $directoryClass = 'class';
    try {
        if (!is_dir($directoryClass)) {
            throw new DirectoryException("Directory 'class' is not exists");
        }

        if (is_writable($directoryClass)) {
            throw new FileException("Directory 'class' is not writable");
        }

        if (!$file = @fopen('test.txt', 'a+')) {
            throw new FileSystemException('System can\'t open test.txt file');
        }

        if (!file_exists('test.txt')) {
            throw new FileLogException("test.txt file not found.");
        }

        if (!isset($eTask41)) {
            throw new BaseException('not a variable');
        }

        fputs($file, date('[H:i:s]') . " done\n");
        fclose($file);
    } catch (DirectoryException $e) {
        echo 'There was a problem with the directory: ' . $e->getMessage() . $e->getTraceAsString();
        error_log("\n\tThere was a problem with the directory: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
    } catch (FileException $e) {
        echo 'There was a problem with the file: ' . $e->getMessage() . $e->getTraceAsString();
        error_log("\n\tThere was a problem with the file: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
    } catch (FileSystemException $e) {
        echo 'File system error: ' . $e->getMessage() . $e->getTraceAsString();
        error_log("\n\tFile system error: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
    } catch (FileLogException $e) {
        echo $e->getMessage() . $e->getTraceAsString();
        error_log("\n\tExeption1: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
    } catch (BaseException $e) {
        echo $e->getMessage() . $e->getTraceAsString();
        error_log("\n\tExeption2: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
    } catch (Error $e) {
        $e->getMessage();
        error_log("\n\tError task 41: " . $e->getMessage(), 3, __DIR__ . "/logs/task.log");
    }
}

register_shutdown_function('shutdown');

/**
 * Task 42
 */
echo '<hr><b style="color:red">Task 42</b><hr>';
echo 'code execution from the beginning to this place ' . (microtime(true) - $start) . ' ms.';

/**
 * Task 43
 */


/**
 * Task 39
 */
echo '<hr><b style="color:red">Task 39</b><hr>';

use App\User as User39;
$obj39 = new User39($_POST);

if($obj39->isAuthorized()) {
    echo "<br><b>User authorized.</b> <a href='?logout=y'>log Out</a>";
} else {
    echo "<br><b>User Not authorized</b>";
}

echo '<hr><b>Registration form:</b><form method="post" name="registration"><input type="hidden" name="action" value="reg">';
echo '<br><label>Email</label><input type="text" name="email">';
echo '<br><label>Name</label><input type="text" name="name">';
echo '<br><label>Phone</label><input type="text" name="phone">';
echo '<br><label>Password</label><input type="password" name="password">';
echo '<br><input type="submit" name="submit" value="Add">';
echo '</form><hr>';

echo '<hr><b>Auth form:</b><form method="post" name="registration"><input type="hidden" name="action" value="auth">';
echo '<br><label>Email</label><input type="text" name="email">';
echo '<br><label>Password</label><input type="password" name="password">';
echo '<br><input type="submit" name="submit" value="Sign In">';
echo '</form><hr>';

/*
- 42, 43 и 39;
- по задаче 39: основная задача в ней - разобраться с исключениями, и если вообще никак не получается написать логику для условий выполнения, то можете облегчить ее по максимуму;
 */


echo '<hr><b style="color:red">HOME WORK #6<BR><BR>Task 40</b><hr>';

/**
 * Task 40
 */

use Task40\Fridge as Fridge40;
use Task40\Microwave as Microwave40;
use Task40\PrivateHouse as PrivateHouse40;
use Task40\ApartmentHouse as ApartmentHouse40;
use Task40\Dugout as Dugout40;

$obj401 = new Microwave40();
$obj402 = new Fridge40();
$obj403 = new Microwave40();
$obj403->openDoor();
$obj404 = new Fridge40();
$obj404->on();

$obj405 = new PrivateHouse40($obj402, $obj401);
$obj406 = new ApartmentHouse40($obj404, $obj403);
$obj407 = new Dugout40();

echo '<pre>';
print_r($obj406);
echo '</pre>';

/**
 * Task 3
 */
echo "<br><br><b>Task 3 Reflection</b><br>#32";
$reflectionClass31 = new ReflectionClass('Task40\ApartmentHouse');
var_dump($reflectionClass31->getName());
var_dump($reflectionClass31->getDocComment());

echo "<br><br>#32";
$reflectionClass32 = new ReflectionMethod('Task31\FiguresCollection', 'getTotalSquare');
var_dump($reflectionClass32->getName());
var_dump($reflectionClass32->getDocComment());

echo "<br><br>#33";
//$reflectionClass33 = new ReflectionParameter('Task30\Disk', 'getPerimeter');
//var_dump($reflectionClass33->getName());
/**
 * TODO
 */
echo "<br><br>#34";
$reflectionClass34 = new ReflectionClass('Task29\Rectangle');
var_dump($reflectionClass34->getName());

echo "<br><br>#35";
$reflectionClass35 = new ReflectionClass('Task28\Quadrate');
var_dump($reflectionClass35->getName());

/**
 * Task 4
 */
echo '<hr><b style="color:red">Task 4</b><hr>';

use Task504\Catalog as Catalog504;

$category = new Catalog504;
$category->enqueue('Childs world');
$category->enqueue('Property');
$category->enqueue('Transport');
$category->enqueue('Spare parts for transport');
$category->enqueue('See all ads in Childrens world');
$category->enqueue('body clothes');
$category->enqueue('Childrens shoes');
$category->enqueue('Baby strollers');
$category->enqueue('Child car seats');
$category->enqueue('Childrens furniture');
$category->enqueue('Toys');
$category->enqueue('Childrens transport');
$category->enqueue('Feeding Supplies');
$category->enqueue('Products for students');
$category->enqueue('PhonesClimatic equipment and accessorees');
$category->enqueue('Computers and accessories');
$category->enqueue('Photo / video');
$category->enqueue('TV / Video');
$category->enqueue('Audio');
$category->enqueue('Games and game consoles');
$category->enqueue('Tablets / Email books and acessories');
$category->enqueue('Laptops and accessories');
$category->enqueue('Home Appliances');
$category->enqueue('Kitchen Appliances');
$category->enqueue('Individual care');
$category->enqueue('Accessories and components');
$category->enqueue('Other electronics');
$category->enqueue('Other parts');
echo '<pre>';
print_r($category);
echo '</pre>';

echo $category->dequeue() . "<br>";
echo $category->dequeue() . "<br>";
echo $category->dequeue() . "<br>";

$category->enqueue('Construction');
$category->enqueue('Financial');
$category->enqueue('Transportation');
echo '<pre>';
print_r($category);
echo '</pre>';
echo '<br>key= ' . $category->key();

/**
 * Task 5
 */
echo '<hr><b style="color:red">Task 5</b><hr>';
echo 'class_implements: ' . get_class($obj401) . '<pre>';
print_r(class_implements($obj401));
echo '</pre>';
echo 'class_implements: ' . get_class($obj402) . '<pre>';
print_r(class_implements($obj402));
echo '</pre>';

echo 'class_parents: ' . get_class($obj403) . '<pre>';
print_r(class_parents($obj401));
echo '</pre>';
echo 'class_parents: ' . get_class($obj404) . '<pre>';
print_r(class_parents($obj402));
echo '</pre>';
/*Задача 5.1: Возьмите любые 2 из ​ SPL функций​ и напишите по 2 примера на каждую,
как ее можно использовать (копировать примеры из оф. доки запрещается).*/

/**
 * Task 6
 */
echo '<hr><b style="color:red">Task 6</b><hr>';
$array506 = array('Home Appliances', 'Kitchen Appliances', 'Individual care', 'Accessories and components', 'Other electronics', 'Other parts');
$iter506 = new ArrayIterator($array506);
foreach ($iter506 as $key => $value) {
    echo $key . ': ' . $value . '<br>';
}

$iter506->natcasesort();
$iter506->append('Apple');
$iter506->append('Yamaha');
$iter506->append('World');
echo '<br>flag= ' . $iter506->getFlags() . ', count= ' . $iter506->count() . '<br>';
echo '<pre>' . print_r($iter506->getArrayCopy());
echo '</pre>';

/**
 * Task 7
 */
echo '<hr><b style="color:red">Task 7</b><hr>';
$array507 = array(
    0 => array('Vegetables', 'Fruit'),
    1 => array('Apricot', 'Orange'),
    2 => array('Banana', 'Cucumber'),
    3 => array('Tomato', 'Pumpkin'),
);
$iterator507 = new RecursiveArrayIterator($array507);
$iterator507->append('append');
$iterator507->next();
echo '<br>flag= ' . $iterator507->getFlags();
echo '<br><pre>';
print_r($iterator507);
echo '</pre>';
$iterator507->offsetUnset(2);
echo '<br><pre>';
print_r($iterator507);
echo '</pre>';

/**
 * Task 8
 */
echo '<hr><b style="color:red">Task 8</b><hr>';
try {
    $dir = new RecursiveDirectoryIterator(__DIR__ . '/vendor/');
    foreach ($dir as $item) {
        if ($item->isDir()) {
            echo 'dir: ' . $item . '<br>';
        }

        if ($item->isFile()) {
            echo 'file: ' . $item . '<br>';
        }
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
echo '<br><br><br>';
try {
    $dir = new DirectoryIterator(__DIR__ . '/vendor/');
    foreach ($dir as $item) {
        if ($item->isDir()) {
            echo 'dir:(' . $item . ')<br>';
            $line = $item->__toString();
            if ($line !== '.' && $line !== '..') {
                $dir1 = new DirectoryIterator(__DIR__ . '/vendor/' . $item);
                foreach ($dir1 as $item1) {
                    if ($item1->isDir()) {
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;dir: ' . $item1 . '<br>';
                    }

                    if ($item1->isFile()) {
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;file: ' . $item1 . '<br>';
                    }
                }
            }
        }

        if ($item->isFile()) {
            echo 'file: ' . $item . '<br>';
        }
    }
} catch (Exception $e) {
    echo $e->getMessage();
}