<?php

namespace App;

use App\BaseException as BaseException;

/**
 * Class FileException
 *
 * @package App
 */
class FileException  extends BaseException
{
}