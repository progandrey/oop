<?php

namespace App;

use App\BaseException as BaseException;

/**
 * Class InitFileNotFoundExeption
 *
 * @package App
 */
class InitFileNotFoundExeption  extends BaseException
{
}