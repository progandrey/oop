<?php

namespace App;

use App\BaseException as BaseException;

/**
 * Class FileSystemException
 *
 * @package App
 */
class FileSystemException  extends BaseException
{
}