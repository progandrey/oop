<?php

namespace App;

/**
 * Class BaseException
 *
 * @package App
 */
class BaseException  extends \Exception
{
}