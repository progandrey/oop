<?php

namespace App;

use App\BaseException as BaseException;

/**
 * Class DirectoryException
 *
 * @package App
 */
class DirectoryException  extends BaseException
{
}