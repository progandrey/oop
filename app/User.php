<?php

namespace App;

/**
 * Class User
 *
 * @param array $error
 * @param array $post
 * @param bool $isAuthorized
 * @param string $email
 * @param string $password
 * @param string $phone
 * @param string $name
 *
 * @package App
 *
 * @property string $current
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class User extends \Exception
{
    private $error = array(
        200 => "Ok",
        201 => "User was created",
        202 => "",
        404 => "User was not found",
        403 => "you have no rights",
        422 => "User is incorrect",
        500 => "Technical workss are carried out on the site",
    );
    private $post;
    private $isAuthorized = false;
    private $email;
    private $password;
    private $phone;
    private $name;

    /**
     * User constructor.
     *
     * @param array $post
     */
    public function __construct(array $post)
    {
        echo __DIR__;
        if(count($post) > 0) {
            echo '<pre>';print_r($_POST);echo '</pre>';
            $this->post = $post;

            /** check params */
            if($this->checkEmail($post['email'])) {
                $this->email = $post['email'];
            }

            if($this->checkPassword($post['password'])) {
                $this->password = $post['password'];
            }

            if($this->checkName($post['name'])) {
                $this->name = $post['name'];
            }

            if($this->checkPhone($post['phone'])) {
                $this->phone = $post['phone'];
            }


            /** Action */
            if($post['action'] == 'reg') {
                $this->reg();
            } elseif ($post['action'] == 'auth') {
                $this->auth();
            }
        }
    }

    /**
     * reg
     */
    public function reg()
    {
        try {
            if (!$file = @fopen('lid.php', 'w+')) {
                throw new FileSystemException('System can\'t open lid.php file');
            }

            if (!file_exists('lid.php')) {
                throw new FileLogException("tlid.php file not found.");
            }

            if (!isset($this->email) && !isset($this->password)) {
                throw new BaseException('not email or password');
            }

            fputs($file, $this->email.','.$this->password.','.date('[H:i:s]') . " ;\n");
            fclose($file);
        } catch (FileSystemException $e) {
            echo 'File system error: ' . $e->getMessage() . $e->getTraceAsString();
            //error_log("\n\tTask39: File system error: " . $e->getMessage(), 3, "task1.log");
        } catch (FileLogException $e) {
            echo $e->getMessage() . $e->getTraceAsString();
            //error_log("\n\tTask39: Exeption1: " . $e->getMessage(), 3, "task1.log");
        } catch (BaseException $e) {
            echo $e->getMessage() . $e->getTraceAsString();
            //error_log("\n\tTask39: Exeption2: " . $e->getMessage(), 3, "task1.log");
        }
    }

    /**
     * auth
     *
     * @return string
     */
    public function auth(): string
    {
        $return = $this->error[200];
        try {
            if (!$file = @fopen('lid.php', 'a+')) {
                throw new FileSystemException('System can\'t open test.txt file');
            }

            if (!file_exists('lid.php')) {
                throw new FileLogException("test.txt file not found.");
            }

            if (!isset($this->email) && !isset($this->password)) {
                throw new BaseException('not email or password');
            }

            // TODO reed
            fclose($file);
        } catch (FileSystemException $e) {
            echo 'File system error: ' . $e->getMessage() . $e->getTraceAsString();
            error_log("\n\tFile system error: " . $e->getMessage(), 3, __DIR__ . "/logs/task41.log");
        } catch (FileLogException $e) {
            echo $e->getMessage() . $e->getTraceAsString();
            error_log("\n\tExeption1: " . $e->getMessage(), 3, __DIR__ . "/logs/task41.log");
        } catch (BaseException $e) {
            echo $e->getMessage() . $e->getTraceAsString();
            error_log("\n\tExeption2: " . $e->getMessage(), 3, __DIR__ . "/logs/task41.log");
        }

        return $return;
    }

    /**
     * check Email
     *
     * @param string $email
     * @return bool
     */
    protected function checkEmail(string $email): bool
    {
        if(preg_match("/[a-zA-Z0-9_\-.]+@[a-zA-Z0-9\-]+\.[a-zA-Z]/", $email)) {
            return true;
        } else {
            echo "Email input format is not correct!";
            return false;
        }
    }

    /**
     * check Password
     *
     * @param string $password
     * @return bool
     */
    protected function checkPassword(string $password): bool
    {
        if(preg_match("/[a-zA-Z0-9_]{1,}/", $password)) {
            return true;
        } else {
            echo "Password input format is not correct!";
            return false;
        }
    }

    /**
     * check Name
     *
     * @param $string name
     * @return bool
     */
    protected function checkName(string $name): bool
    {
        if(strlen($name) > 1) {
            return true;
        } else {
            echo "The name must be greater than the 1st character";
            return false;
        }
    }

    /**
     * check Phone
     *
     * @param string $phone
     * @return bool
     */
    protected function checkPhone(string $phone): bool
    {
        if(preg_match("/[0-9]{1,}/", $phone)) {
            return true;
        } else {
            echo "Invalid phone entered";
            return false;
        }
    }

    /**
     * is Authorized
     *
     * @return bool
     */
    public function isAuthorized(): bool
    {
        return $this->isAuthorized;
    }
}