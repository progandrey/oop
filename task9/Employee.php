<?php

namespace Task9;

/**
 * Class Employee
 *
 * @package Task9
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 * @param float $salary
 */
class Employee
{
    private $name;
    private $age;
    private $salary;

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Set age
     *
     * @param int $age
     */
    public function setAge(int $age)
    {
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    /**
     * Set salary
     *
     * @param float $salary
     */
    public function setSalary(float $salary)
    {
        $this->salary = $salary;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * Get salary
     *
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary . ' дол.';
    }

    /**
     * Check age correct
     *
     * @param int $age
     *
     * @return bool
     */
    private function isAgeCorrect(int $age): bool
    {
        if ($age > 1 && $age < 100) {
            return true;
        } else {
            return false;
        }
    }
}