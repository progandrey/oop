<?php

namespace Task26;

/**
 * Interface UserInterface
 *
 * @package Task26
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface UserInterface
{
    /**
     * UserInterface constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age);

    /**
     * get Name
     *
     * @return mixed
     */
    public function getName();

    /**
     * get Age
     *
     * @return mixed
     */
    public function getAge();
}