<?php

namespace Task26;

use Task26\UserInterface as UserInterface;

/**
 * Class User
 *
 * @package Task26
 *
 * @param string $name
 * @param int $age
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class User implements UserInterface
{
    private $name;
    private $age;

    /**
     * User constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}