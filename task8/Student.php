<?php

namespace Task8;

/**
 * Class Student
 *
 * @package Task8
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $course
 * @param string $courseAdministrator
 */
class Student
{
    public $name;
    public $course;
    private $courseAdministrator;

    /**
     * Student constructor.
     *
     * @param string $name
     * @param int $course
     */
    public function __construct(string $name, int $course)
    {
        $this->name = $name;
        $this->course = $course;
    }

    /**
     * Transfer to next course
     */
    public function transferToNextCourse()
    {
        if ($this->isCourseCorrect()) {
            $this->course++;
        }
    }

    /**
     * Check is cours correct
     *
     * @return bool
     */
    private function isCourseCorrect(): bool
    {
        if ($this->course > 0 && $this->course < 5) {
            return true;
        } else {
            echo 'Обучение длится всего 5 курсов.';
            return false;
        }
    }

    /**
     * Set course administrator
     *
     * @param string $nameAdministrator
     */
    public function setCourseAdministrator(string $nameAdministrator)
    {
        $this->courseAdministrator = $nameAdministrator;
    }

    /**
     * Get course administrator
     * @return string
     */
    public function getCourseAdministrator(): string
    {
        return $this->courseAdministrator;
    }
}