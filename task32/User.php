<?php

namespace Task32;

use Task32\Helper as Helper;

/**
 * Class User
 *
 * @package Task32
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class User
{
    /**
     * use Helper
     */
    use Helper;
}