<?php

namespace Task32;

use Task32\Helper as Helper;

/**
 * Class Country
 *
 * @package Task32
 *
 * @param int $population
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Country
{
    private $population;

    /**
     * get Population
     * 
     * @return int
     */
    public function getPopulation(): int 
    {
        return $this->population;
    }

    /**
     * use Helper
     */
    use Helper;
}