<?php

namespace Task32;

/**
 * Trait Helper
 *
 * @package Task32
 *
 * @param string $name
 * @param int $age
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
trait Helper
{
    private $name;
    private $age;

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}