<?php

namespace Task11;

/**
 * Class Employee
 *
 * @package Task11
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 * @param float $salary
 */
class Employee
{
    public $name;
    public $age;
    public $salary;

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param int $age
     * @param float $salary
     */
    public function __construct(string $name, int $age, float $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }
}