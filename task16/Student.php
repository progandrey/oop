<?php

namespace Task16;

/**
 * Class Student
 *
 * @package Task16
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param float $scholarship
 */
class Student
{
    public $name;
    public $scholarship;

    /**
     * Student constructor.
     *
     * @param string $name
     * @param float $scholarship
     */
    public function __construct(string $name, float $scholarship)
    {
        $this->name = $name;
        $this->scholarship = $scholarship;
    }
}