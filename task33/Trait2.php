<?php

namespace Task33;

/**
 * Trait Trait2
 *
 * @package Task33
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
trait Trait2
{
    /**
     * method2
     *
     * @return int
     */
    private function method2(): int
    {
        return 2;
    }
}