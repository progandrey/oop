<?php

namespace Task33;

/**
 * Trait Trait3
 *
 * @package Task33
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
trait Trait3
{
    /**
     * method3
     *
     * @return int
     */
    private function method3(): int
    {
        return 3;
    }
}