<?php

namespace Task33;

use Task33\Trait1 as Trait1;
use Task33\Trait2 as Trait2;
use Task33\Trait3 as Trait3;


/**
 * Class Test
 *
 * @package Task33
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Test
{
    /**
     * use Trait1, Trait2, Trait3;
     */
    use Trait1, Trait2, Trait3;

    /**
     * get Sum
     *
     * @return int
     */
    public function getSum(): int
    {
        return $this->method1() + $this->method2() + $this->method3();
    }
}