<?php

namespace Task12;

/**
 * Class City
 *
 * @package Task12
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $foundation
 * @param string $population
 * @param array $props
 */
class City
{
    public $name;
    public $foundation;
    public $population;
    public $props = ['name', 'foundation', 'population'];

    /**
     * City constructor.
     *
     * @param string $name
     * @param string $foundation
     * @param int $population
     */
    public function __construct(string $name, string $foundation, int $population)
    {
        $this->name = $name;
        $this->foundation = $foundation;
        $this->population = $population;
    }

    /**
     * Get props value
     */
    public function getPropsValue()
    {
        foreach ($this->props AS $value) {
            echo "<br>$value = " . $this->$value;
        }
    }
}