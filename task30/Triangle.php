<?php

namespace Task30;

use Task30\Figure as Figure;

/**
 * Class Triangle
 *
 * @package Task30
 *
 * @param float $a
 * @param float $b
 * @param float $c
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Triangle extends Figure
{
    private $a;
    private $b;
    private $c;

    /**
     * Triangle constructor.
     *
     * @param float $a
     * @param float $b
     * @param float $c
     */
    public function __construct(float $a, float $b, float $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float
    {
        return $this->a + $this->b + $this->c;
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        $p = $this->getPerimeter() / 2;
        return sqrt($p * ($p - $this->a) * ($p - $this->b) * ($p - $this->c));
    }
}