<?php

namespace Task30;

/**
 * Class Figure
 *
 * @package Task30
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
abstract class Figure
{
    /**
     * get Square
     *
     * @return float
     */
    abstract public function getSquare(): float ;

    /**
     * get Perimeter
     *
     * @return float
     */
    abstract public function getPerimeter(): float ;

    /**
     * get Sum Square And Perimeter
     *
     * @return float
     */
    public function getSumSquareAndPerimeter(): float
    {
        return $this->getSquare() + $this->getPerimeter();
    }
}