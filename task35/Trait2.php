<?php

namespace Task35;

use Task35\Trait1 as Trait1;

/**
 * Trait Trait2
 *
 * @package Task35
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
trait Trait2
{
    /**
     * use Trait1
     */
    use Trait1;

    /**
     * method3
     *
     * @return int
     */
    private function method3(): int
    {
        return 3;
    }
}