<?php

namespace Task10;

/**
 * Class Employee
 *
 * @package Task10
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param string $surname
 * @param float $salary
 */
class Employee
{
    private $name;
    private $surname;
    private $salary;

    /**
     * Set surname
     *
     * @param float $salary
     */
    public function setSalary(float $salary)
    {
        $this->salary = $salary;
    }

    /**
     * Get name
     *
     * @return int
     */
    public function getName(): int
    {
        return $this->name;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * Get salary
     *
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary . ' дол.';
    }
}