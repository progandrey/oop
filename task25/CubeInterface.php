<?php

namespace Task25;

/**
 * Interface CubeInterface
 *
 * @package Task25
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface CubeInterface
{
    /**
     * CubeInterface constructor.
     *
     * @param float $a
     */
    public function __construct(float $a);

    /**
     * get Volume
     *
     * @return mixed
     */
    public function getVolume();

    /**
     * get Square
     *
     * @return mixed
     */
    public function getSquare();
}