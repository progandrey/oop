<?php

namespace Task25;

use Task25\CubeInterface as CubeInterface;

/**
 * Class Cube
 *
 * @package Task25
 *
 * @param float $a
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Cube implements CubeInterface
{
    private $a;

    /**
     * Cube constructor.
     *
     * @param float $a
     */
    public function __construct(float $a)
    {
        $this->a = $a;
    }

    /**
     * get Volume
     *
     * @return float
     */
    public function getVolume()
    {
        return pow($this->a, 3);
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        return 6 * pow($this->a, 2);
    }
}