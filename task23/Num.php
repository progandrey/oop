<?php

namespace Task23;

/**
 * Class Num
 *
 * @package Task23
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param int $num1
 * @param int $num2
 * @param int $num3
 * @param int $num4
 */
class Num
{
    public static $num1 = 2;
    public static $num2 = 3;
    private static $num3 = 3;
    private static $num4 = 5;
    const PI = 3.14;
    const PR2 = 'a * b';

    /**
     * get Sum
     *
     * @param int $num3
     * @param int $num4
     *
     * @return int
     */
    public static function getSum(): int
    {
        return self::$num3 + self::$num4;
    }

    /**
     * get Constant
     *
     * @return string
     */
    public static function getConstant(): string
    {
        return self::PR2;
    }
}