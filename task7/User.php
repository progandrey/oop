<?php

namespace Task7;

/**
 * Class User
 *
 * @package Task7
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 */
class User
{
    public $name;
    public $age;

    /**
     * User constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        if ($this->validator($name, $age)) {
            $this->name = $name;
            $this->age = $age;
        }
    }

    /**
     * Set age
     *
     * @param int $age
     */
    public function setAge(int $age)
    {
        if ($this->validator('', $age)) {
            $this->age = $age;
        }
    }

    /**
     * Add age
     *
     * @param int $add
     */
    public function addAge(int $add)
    {
        if ($this->validator('', $this->age + $add)) {
            $this->age = $this->age + $add;
        }
    }

    /**
     * Sub age
     *
     * @param int $sub
     */
    public function subAge(int $sub)
    {
        if ($this->validator('', $this->age - $sub)) {
            $this->age = $this->age - $sub;
        }
    }

    /**
     * Validator
     *
     * @param string $name
     * @param int $age
     *
     * @return bool
     */
    private function validator(string $name, int $age): bool
    {
        $return = true;

        if (strlen($name) > 0 && strlen($name) < 2) {
            echo "Имя .$name содержыт меньше 2-х символов";
            $return = false;
        }

        if (isset($age) && $age < 2) {
            echo "Ваш возраст $age меньше 2-х лет.";
            $return = false;
        }

        if (isset($age) && $age > 120) {
            echo "Вашему возрасту $age можна позавидовать но скорей вы ошыблись).";
            $return = false;
        }

        return $return;
    }
}