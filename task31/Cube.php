<?php

namespace Task31;

use Task31\FigureInterface as FigureInterface;

/**
 * Class Cube
 *
 * @param float $a
 *
 * @package Task31
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Cube implements FigureInterface
{
    private $a;

    /**
     * Cube constructor.
     *
     * @param float $a
     */
    public function __construct(float $a)
    {
        $this->a = $a;
    }

    /**
     * get Volume
     *
     * @return float
     */
    public function getVolume(): float
    {
        return pow($this->a, 3);
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        return 6 * pow($this->a, 2);
    }

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float
    {
        return 12 * $this->a;
    }

    /**
     * get Sum Square And Perimeter
     *
     * @return float
     */
    public function getSumSquareAndPerimeter(): float
    {
        return $this->getSquare() + $this->getPerimeter();
    }
}