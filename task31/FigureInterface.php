<?php

namespace Task31;

/**
 * Interface FigureInterface
 *
 * @package Task31
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface FigureInterface
{
    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float ;

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float ;

    /**
     * get Sum Square And Perimeter
     *
     * @return float
     */
    public function getSumSquareAndPerimeter(): float ;
}