<?php

namespace Task31;

/**
 * Class FiguresCollection
 *
 * @package Task31
 *
 * @param array $triangles
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class FiguresCollection
{
    private $triangles = [];

    /**
     * add Figure
     *
     * @param Triangle $triangle
     */
    public function addFigure(Triangle $triangle)
    {
        $this->triangles[] = $triangle;
    }

    /**
     * get Total Square
     *
     * @return float
     */
    public function getTotalSquare(): float
    {
        $sum = 0;

        foreach ($this->triangles as $triangle) {
            $sum += $triangle->getSquare();
        }

        return $sum;
    }
}