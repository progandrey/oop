<?php

namespace Task31;

use Task31\FigureInterface as FigureInterface;

/**
 * Class Rectangle
 *
 * @package Task31
 *
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $d
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Rectangle implements FigureInterface
{
    private $a;
    private $b;
    private $c;
    private $d;

    /**
     * Rectangle constructor.
     *
     * @param float $a
     * @param float $b
     * @param float $c
     * @param float $d
     */
    public function __construct(float $a, float $b, float $c, float $d)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->d = $d;
    }

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float
    {
        return $this->a + $this->b + $this->c + $this->d;
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        return $this->a * $this->b;
    }

    /**
     * get A
     *
     * @return float
     */
    public function getA(): float
    {
        return $this->a;
    }

    /**
     * get B
     *
     * @return float
     */
    public function getB(): float
    {
        return $this->b;
    }

    /**
     * get C
     *
     * @return float
     */
    public function getC(): float
    {
        return $this->c;
    }

    /**
     * get D
     *
     * @return float
     */
    public function getD(): float
    {
        return $this->d;
    }

    /**
     * get Sum Square And Perimeter
     *
     * @return float
     */
    public function getSumSquareAndPerimeter(): float
    {
        return $this->getSquare() + $this->getPerimeter();
    }
}