<?php

namespace Task22;

/**
 * Class ArraySumHelper
 *
 * @package Task22
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param array $a
 * @param int $b
 */
class ArraySumHelper
{
    public $a = [];
    public $b;

    /**
     * get Sum
     *
     * @param array $a
     * @param int $b
     *
     * @return float
     */
    public static function getSum(array $a, int $b): float
    {
        $sum = 0;
        foreach ($a as $item) {
            $sum += ($item * $b);
        }
        return $sum;
    }
}