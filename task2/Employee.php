<?php

namespace Task2;

/**
 * Class Employee
 *
 * @package Task2
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 * @param float $salary
 */
class Employee
{
    public $name;
    public $age;
    public $salary;

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * Get salary
     *
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }

    /**
     * Check age
     *
     * @return bool
     */
    public function checkAge(): bool
    {
        if ($this->age > 18) {
            return true;
        } else {
            return false;
        }
    }
}