<?php

namespace Task20;
/**
 * Class Student
 *
 * @package Task20
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $course
 * @param string $courseAdministrator
 */
class Student
{
    public $name;
    public $course;
    private $courseAdministrator;

    /**
     * Student constructor.
     *
     * @param string $name
     * @param int $course
     */
    public function __construct(string $name, int $course)
    {
        if ($this->isName($name)) {
            $this->name = $name;
        }

        if ($this->isCourseCorrect($course)) {
            $this->course = $course;
        }
    }

    /**
     * Transfer to next course
     */
    public function transferToNextCourse()
    {
        if ($this->isCourseCorrect($this->course)) {
            $this->course++;
        }
    }

    /**
     * Check is cours correct
     *
     * @param int $course
     *
     * @return bool
     */
    private function isCourseCorrect(int $course): bool
    {
        if ($course > 0 && $course < 5) {
            return true;
        } else {
            echo 'Обучение длится всего 5 курсов.';
            return false;
        }
    }

    /**
     * Check is name correct
     *
     * @param string $name
     *
     * @return bool
     */
    private function isName(string $name): bool
    {
        if (strlen($name) > 1 && strlen($name) <= 50) {
            return true;
        } else {
            echo 'Введите имя больше 1-го и не превышающе 50 символов';
            return false;
        }
    }

    /**
     * Check is sur name correct
     *
     * @param string $surName
     *
     * @return bool
     */
    protected function isSurName(string $surName): bool
    {
        if (strlen($surName) > 1) {
            return true;
        } else {
            echo "Фамилия ($surName) должна быть больше 2-х символов";
            return false;
        }
    }

    /**
     * Set course administrator
     *
     * @param string $nameAdministrator
     */
    public function setCourseAdministrator(string $nameAdministrator)
    {
        $this->courseAdministrator = $nameAdministrator;
    }

    /**
     * Get course administrator
     *
     * @return string
     */
    public function getCourseAdministrator(): string
    {
        return $this->courseAdministrator;
    }
}