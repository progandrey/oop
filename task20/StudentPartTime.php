<?php

namespace Task20;

use Task20\Student as Student;

/**
 * Class StudentPartTime
 *
 * @package Task20
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $surName
 */
class StudentPartTime extends Student
{
    public $surName;

    /**
     * StudentPartTime constructor.
     *
     * @param string $name
     * @param int $course
     * @param string $surName
     */
    public function __construct(string $name, int $course, string $surName)
    {
        parent::__construct($name, $course);
        if ($this->isSurNameCorrect($surName)) {
            $this->surName = $surName;
        }
    }

    /**
     * Check is sur name correct
     *
     * @param string $surName
     *
     * @return bool
     */
    public function isSurNameCorrect(string $surName): bool
    {
        if (strlen($surName) <= 20) {
            if(parent::isSurName($surName))
                return true;
            else
                return false;
        } else {
            echo "<br>Фамилия ($surName) должна быть не больше 20-тх символов";
            return false;
        }
    }
}