<?php

namespace Task3;

/**
 * Class User
 *
 * @package Task3
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 */
class User
{
    public $name;
    public $age;

    /**
     * User constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * Set age
     *
     * @param int $age
     */
    public function setAge(int $age)
    {
        if ($age >= 18) {
            $this->age = $age;
        }
    }
}