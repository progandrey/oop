<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'vendor/autoload.php';

/*
- ребята, кто не был на Базовом курсе делают задачи с 24 по 31 включительно (одно из этих заданий разрешаю пропустить), плюс задачи 32, 33, 34, 35, 38, 40, 41, 42, 43 и 39;
- все остальные делают задачи 32, 33, 34, 35, 38, 40, 41, 42, 43 и 39;
- по задаче 39: основная задача в ней - разобраться с исключениями, и если вообще никак не получается написать логику для условий выполнения, то можете облегчить ее по максимуму;
 */

/**
 * Task 24
 */

use Task24\User as User24;

$obj24 = new User24();

echo "<br><br><b>Task 24</b><br>";
$obj24->setName('Andrey');
$obj24->setAge(25);
echo '<br>Name: ' . $obj24->getName();
echo '<br>Age: ' . $obj24->getAge();

/**
 * Task 25
 */

use Task25\Cube as Cube25;

$obj25 = new Cube25(3);
echo "<br><br><b>Task 25</b><br>";
echo '<br>Volume: ' . $obj25->getVolume();
echo '<br>Square: ' . $obj25->getSquare();

/**
 * Task 26
 */

use Task26\User as User26;

$obj26 = new User26('Andrey', 83);
echo "<br><br><b>Task 26</b><br>";
echo '<br>Name: ' . $obj26->getName();
echo '<br>Age: ' . $obj26->getAge();

/**
 * Task 27
 */

use Task27\Employee as Employee27;

$obj27 = new Employee27('Andrey', 83);
echo "<br><br><b>Task 27</b><br>";
$obj27->setSalary(400);
echo '<br>Name: ' . $obj27->getName();
echo '<br>Age: ' . $obj27->getAge();
echo '<br>Salary: ' . $obj27->getSalary();

/**
 * Task 28
 */

use Task28\Cube as Cube28;
use Task28\Figure3dInterface as Figure3dInterface;
use Task28\FigureInterface as FigureInterface;
use Task28\Rectangle as Rectangle28;
use Task28\Quadrate as Quadrate28;

// 28.4
$obj2811 = new Cube28(3);
$obj1812 = new Cube28(2);

$obj2821 = new Rectangle28(4, 7);
$obj2822 = new Rectangle28(8, 3);

$obj2831 = new Quadrate28(4, 7, 3, 6, 12, 43);
$obj2832 = new Quadrate28(2, 5, 7, 3, 12, 65);

$arr28 = array();
$arr28[] = $obj2811;
$arr28[] = $obj2832;
$arr28[] = $obj1812;
$arr28[] = $obj2831;
$arr28[] = $obj2822;
$arr28[] = $obj2821;

// 28.5
echo "<br><br><b>Task 28</b><br>";
echo "<br><br>FigureInterface:<br>";

foreach ($arr28 AS $value) {
    if ($value instanceof FigureInterface) {
        echo 'Square: ' . $value->getSquare() . '<br>';
    }
}

// 28.6
echo "<br>#28.6:<br>";
foreach ($arr28 AS $value) {
    if ($value instanceof FigureInterface) {
        echo 'Square: ' . $value->getSquare() . '<br>';
    }

    if ($value instanceof Figure3dInterface) {
        echo 'SurfaceSquare: ' . $value->getSurfaceSquare() . ' - Volume: ' . $value->getVolume() . '<br>';
    }
}

/**
 * Task 29 - 33
 */

/**
 * Task 34
 */

use Task34\Test as Test34;

$obj34 = new Test34();

echo "<br><br><b>Task 34</b><br>";
echo $obj34->getSum();

/**
 * Task 35
 */

use Task35\Test as Test35;

$obj35 = new Test35();

echo "<br><br><b>Task 35</b><br>";
echo $obj35->getSum();

/**
 * Task 38
 */
echo "<br><br><b>Task 38</b><br>";

use App\BaseException as BaseException;
use App\FileLogException as FileLogException;
use App\InitFileNotFoundExeption as InitFileNotFoundExeption;

try {
    $init_file_path = 'init.php';
    if (!file_exists($init_file_path)) {
        throw new InitFileNotFoundExeption("Init file not found.");
    }
} catch (InitFileNotFoundExeption $e) {
    echo '38.1: ' . $e->getMessage();
    error_log("\n\tExeption1: ".$e->getMessage(), 3, "my-errors.log");
} catch (Error $e) {
    echo $e->getMessage();
}

//2
$message = 'Trace: ';

try {
    if (!isset($one)) {
        throw new FileLogException($message, 0, $e);
    }
    echo $one;
} catch (FileLogException $e) {
    echo '<br>38.2 ' . $e->getMessage() . $e->getTraceAsString();
    error_log("\n\tExeption2: ".$e->getMessage(), 3, "my-errors.log");
} catch (Error $e) {
    $e->getMessage();
}

//3
try {
    $b = array();
    if (!isset($b['a'])) {
        throw new BaseException("not found \$b['a']. <br><br>");
    }
    $b['a'];
} catch (BaseException $e) {
    echo "<br>38.3 Base Exception: " . $e->getMessage();
} catch (Error $e) {
    echo $e->getMessage();
}

//use Task_8\Student as Student8;
/*$obj8 = new Student8('Ivan', 4);
$obj8->setCourseAdministrator('Anton');
echo $obj8->courseAdministrator = 'Filip';*/
//use Task_7\User as User7;
/*$obj7 = new User7('li', 3);
$obj7->validator('li', 5);
*/

/*try {
    $init_file_path = 'init.php';
    if (!file_exists($init_file_path)) {
        throw new InitFileNotFoundExeption("Init file not found. ");
    }
} catch (InitFileNotFoundExeption $e) {
    echo $e->getMessage();
} catch (Error $e) {
    echo $e->getMessage();
}*/

//
/**
 * Task 40
 */
use Task40\Fridge as Fridge40;
use Task40\Microwave as Microwave40;
use Task40\PrivateHouse as PrivateHouse40;
use Task40\ApartmentHouse as ApartmentHouse40;
use Task40\Dugout as Dugout40;

// 28.4
$obj401 = new Microwave40();
$obj402 = new Fridge40();
$obj403 = new PrivateHouse40();
$obj404 = new ApartmentHouse40();
$obj405 = new Dugout40();


/**
 * Task 41
 */


/**
 * Task 42
 */


/**
 * Task 43
 */


/**
 * Task 39
 */


















