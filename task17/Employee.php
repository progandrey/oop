<?php

namespace Task17;

use Task17\User as User;

/**
 * Class Employee
 *
 * @package Task17
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param float $salary
 */
class Employee extends User
{
    public $salary;

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param string $surname
     * @param float $salary
     */
    public function __construct(string $name, string $surname, float $salary)
    {
        parent::__construct($name, $surname);
        $this->salary = $salary;
    }
}