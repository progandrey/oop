<?php

namespace Task17;

/**
 * Class City
 *
 * @package Task17
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $population
 */
class City
{
    public $name;
    public $population;

    /**
     * City constructor.
     *
     * @param string $name
     * @param int $population
     */
    public function __construct(string $name, int $population)
    {
        $this->name = $name;
        $this->population = $population;
    }
}