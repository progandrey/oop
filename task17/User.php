<?php

namespace Task17;

/**
 * Class User
 *
 * @package Task17
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param string $surname
 */
class User
{
    public $name;
    public $surname;

    /**
     * User constructor.
     *
     * @param string $name
     * @param string $surname
     */
    public function __construct(string $name, string $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }
}