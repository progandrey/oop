<?php

namespace Task24;

use Task24\UserInterface as UserInterface;

/**
 * Class User
 *
 * @package Task24
 *
 * @param string $name
 * @param int $age
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class User implements UserInterface
{
    private $name;
    private $age;

    /**
     * set Name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * set Age
     *
     * @param int $age
     */
    public function setAge(int $age)
    {
        $this->age = $age;
    }

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}