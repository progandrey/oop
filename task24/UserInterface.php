<?php

namespace Task24;

/**
 * Interface UserInterface
 *
 * @package Task24
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface UserInterface
{
    /**
     * set Name
     *
     * @param string $name
     */
    public function setName(string $name);

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string ;

    /**
     * set Age
     *
     * @param int $age
     */
    public function setAge(int $age);

    /**
     * get Age
     *
     * @return int
     */
    public function getAge(): int ;
}
