<?php

namespace Task18;

use Task18\Post as Post;

/**
 * Class Employee
 *
 * @package Task18
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param string $surname
 * @param array $post
 */
class Employee
{
    private $name;
    private $surname;
    private $post = [];

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param string $surname
     * @param \Task18\Post $position
     */
    public function __construct(string $name, string $surname, Post $position)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post[] = $position;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * Get Post Params
     *
     * @return string
     */
    public function getPostParams(): string
    {
        $return = '';
        foreach ($this->post as $position) {
            $return .= '<br>name: ' . $position->getName();

            $return .= '<br>salary: ' . $position->getSalary();
        }

        return $return;
    }

    /**
     * #18.8
     *
     * change Post
     *
     * @param \Task18\Post $salary
     * @param string salary
     */
    public function changePost (Post $salary, string $name)
    {
        $salary->setName($name);
    }
}