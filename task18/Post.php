<?php

namespace Task18;

/**
 * Class Post
 *
 * @package Task18
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param float $salary
 */
class Post
{
    private $name;
    private $salary;

    /**
     * Post constructor.
     *
     * @param string $name
     * @param float $salary
     */
    public function __construct(string $name, float $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * set Name
     *
     * @param $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * get Salary
     *
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}