<?php

namespace Task5;

/**
 * Class Rectangle
 *
 * @package Task5
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param int $width
 * @param int $height
 */
class Rectangle
{
    public $width;
    public $height;

    /**
     * Rectangle constructor.
     * @param int $width
     * @param int $height
     */
    public function __construct(int $width, int $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Get square
     *
     * @return int
     */
    public function getSquare(): int
    {
        return $this->width * $this->height;
    }

    /**
     * Get perimeter
     *
     * @return int
     */
    public function getPerimeter(): int
    {
        return ($this->width + $this->height) * 2;
    }
}