<?php

namespace Task28;

/**
 * Interface FigureInterface
 *
 * @package Task28
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
interface FigureInterface
{
    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float ;

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float ;
}