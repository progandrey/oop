<?php

namespace Task28;

use Task28\FigureInterface as FigureInterface;

/**
 * Class Quadrate
 *
 * @package Task28
 *
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $d
 * @param int $u1
 * @param int $u2
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Quadrate implements FigureInterface
{
    private $a;
    private $b;
    private $c;
    private $d;
    private $u1;
    private $u2;

    /**
     * Quadrate constructor.
     *
     * @param float $a
     * @param float $b
     * @param float $c
     * @param float $d
     * @param int $u1
     * @param int $u2
     */
    public function __construct(float $a, float $b, float $c, float $d, int $u1, int $u2)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->d = $d;
        $this->u1 = $u1;
        $this->u2 = $u2;
    }

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float
    {
        return $this->a + $this->b + $this->c + $this->d;
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        $p = $this->getPerimeter()/2;
        $arg1 = ($p - $this->a) * ($p - $this->b) * ($p - $this->c) * ($p - $this->d);
        $arg2 = $this->a * $this->b * $this->c * $this->d;
        $arg3 = cos(($this->u1+$this->u2)/2);
        return sqrt($arg1 - ($arg2 * pow($arg3, 2)));
    }
}