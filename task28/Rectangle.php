<?php

namespace Task28;

use Task28\FigureInterface as FigureInterface;

/**
 * Class Rectangle
 *
 * @package Task28
 *
 * @param float $a
 * @param float $b
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Rectangle implements FigureInterface
{
    private $a;
    private $b;

    /**
     * Rectangle constructor.
     *
     * @param float $a
     * @param float $b
     */
    public function __construct(float $a, float $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    /**
     * get Perimeter
     *
     * @return float
     */
    public function getPerimeter(): float
    {
        return 2 * ($this->a + $this->b);
    }

    /**
     * get Square
     *
     * @return float
     */
    public function getSquare(): float
    {
        return $this->a * $this->b;
    }
}