<?php

namespace Task28;

use Task28\Figure3dInterface as Figure3dInterface;

/**
 * Class Cube
 *
 * @package Task28
 *
 * @param float $a
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 */
class Cube implements Figure3dInterface
{
    private $a;

    /**
     * Cube constructor.
     *
     * @param float $a
     */
    public function __construct(float $a)
    {
        $this->a = $a;
    }

    /**
     * get Volume
     *
     * @return float
     */
    public function getVolume(): float
    {
        return pow($this->a, 3);
    }

    /**
     * get Surface Square
     *
     * @return float
     */
    public function getSurfaceSquare(): float
    {
        return 6 * pow($this->a, 2);
    }
}