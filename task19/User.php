<?php

namespace Task19;

/**
 * Class User
 *
 * @package Task19
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param int $age
 * @param string $driver
 * @param string $employee
 * @param string $programmer
 */
class User
{
    public $name;
    public $age;
    protected $driver;
    public $employee;
    public $programmer;

    /**
     * User constructor.
     *
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * Set age
     *
     * @param int $age
     */
    public function setAge(int $age)
    {
        if ($age >= 18) {
            $this->age = $age;
        }
    }

    /**
     * get Driver
     *
     * @return object
     */
    public function getDriver(): object
    {
        return $this->driver;
    }

    /**
     * set Driver
     *
     * @param object $driver
     */
    public function setDriver(object $driver)
    {
        $this->driver = $driver;
    }

    /**
     * get Employee
     *
     * @return object
     */
    public function getEmployee(): object
    {
        return $this->employee;
    }

    /**
     * set Employee
     *
     * @param object $employee
     */
    public function setEmployee(object $employee)
    {
        $this->employee = $employee;
    }

    /**
     * get Programmer
     *
     * @return object
     */
    public function getProgrammer(): object
    {
        return $this->programmer;
    }

    /**
     * set Programmer
     *
     * @param object $programmer
     */
    public function setProgrammer(object $programmer)
    {
        $this->programmer = $programmer;
    }
}