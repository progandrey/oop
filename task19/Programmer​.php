<?php

namespace Task19;

use Task19\Employee as Employee;

/**
 * Class Programmer
 *
 * @package Task19
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param array $langs
 */
class Programmer extends Employee
{
    private $langs = array();

    /**
     * Get Langs
     *
     * @return array
     */
    public function getLangs(): array
    {
        return $this->langs;
    }

    /**
     * Set Langs
     *
     * @param string $langs
     */
    public function setLangs(string $langs)
    {
        $this->langs[] = $langs;
    }
}