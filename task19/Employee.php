<?php

namespace Task19;

use Task19\User as User;

/**
 * Class Employee
 *
 * @package Task19
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $surname
 */
class Employee extends User
{
    private $surname;

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param string $surname
     * @param int $age
     */
    public function __construct(string $name, int $age, string $surname)
    {
        parent::__construct($name, $age);
        $this->surname = $surname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }
}