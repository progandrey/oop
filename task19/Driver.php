<?php

namespace Task19;

use Task19\Employee as Employee;

/**
 * Class Driver?
 *
 * @package Task19
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param int $drivingExperience
 * @param string $drivingCategory
 */
class Driver extends Employee
{
    private $drivingExperience;
    private $drivingCategory;

    /**
     * Get driving experience
     *
     * @return int
     */
    public function getDrivingExperience(): int
    {
        return $this->drivingExperience;
    }

    /**
     * Set driving experience
     *
     * @param int $drivingExperience
     */
    public function setDrivingExperience(int $drivingExperience)
    {
        $this->drivingExperience = $drivingExperience;
    }

    /**
     * Get driving category
     *
     * @return string
     */
    public function getDrivingCategory(): string
    {
        return $this->drivingCategory;
    }

    /**
     * Set driving category
     *
     * @param string $drivingCategory
     */
    public function setDrivingCategory(string $drivingCategory)
    {
        $this->drivingCategory = $drivingCategory;
    }
}