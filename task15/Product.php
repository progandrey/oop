<?php

namespace Task15;

/**
 * Class Product
 *
 * @package Task15
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param float $price
 * @param int $quantity
 */
class Product
{
    private $name;
    private $price;
    private $quantity;

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Get Quantity
     *
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * ALL price
     *
     * @return float|int
     */
    public function getCost(): float
    {
        return ($this->price * $this->quantity);
    }
}