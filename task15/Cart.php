<?php

namespace Task15;

/**
 * Class Cart
 *
 * @package Task15
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index1_23.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param array $products
 */
class Cart
{
    private $products = [];

    /**
     * Add product
     *
     * @param Product $products
     */
    public function add(Product $products)
    {
        $this->products[$products->getName()] = $products;
    }

    /**
     * Delete product
     *
     * @param string $name
     */
    public function remove(string $name)
    {
        unset($this->products[$name]);
    }

    /**
     * Get total cost
     *
     * @return float
     */
    public function getTotalCost(): float
    {
        $sum = 0;
        foreach ($this->products as $products) {
            $sum += $products->getPrice();
        }

        return $sum;
    }

    /**
     * Get total quantity
     *
     * @return int
     */
    public function getTotalQuantity(): int
    {
        $sum = 0;
        foreach ($this->products as $products) {
            $sum += $products->getQuantity();
        }

        return $sum;
    }

    /**
     * Get total quantity
     *
     * @return float
     */
    public function getAvgPrice(): float
    {
        return ($this->getTotalCost() / $this->getTotalQuantity());
    }
}