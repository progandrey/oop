<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'vendor/autoload.php';

/**
 * Task 1
 */

use Task1\Employee as Employee1;

$obj1 = new Employee1();
$obj1->name = 'Иван';
$obj1->age = 25;
$obj1->salary = 1000;

$obj12 = new Employee1();
$obj12->name = 'Вася';
$obj12->age = 26;
$obj12->salary = 2000;

echo "Задание 1<br>Сумму зарплат Ивана и Васи = " . ($obj1->salary + $obj12->salary);
echo "<br>Сумму возрастов Ивана и Васи = " . ($obj1->age + $obj12->age);

/**
 * Task 2
 */

use Task2\Employee as Employee2;

$obj2 = new Employee2();
$obj2->name = 'Иван';
$obj2->age = 25;
$obj2->salary = 1000;

$obj22 = new Employee2();
$obj22->name = 'Вася';
$obj22->age = 26;
$obj22->salary = 2000;

echo "<br><br>Задание 2<br>Сумму зарплат Ивана и Васи = " . ($obj2->getSalary() + $obj22->getSalary());

/**
 * Task 3
 */

use Task3\User as User3;

$obj3 = new User3('Коля', 25);
echo "<br><br>Задание 3<br>возраст = " . $obj3->age;
$obj3->setAge(30);

echo "<br>Новый возраст Коли = " . $obj3->age;
$obj3->setAge(17);
echo "<br>возраст 17= " . $obj3->age;

$obj3->setAge(31);
echo "<br>возраст 31= " . $obj3->age;

/**
 * Task 4
 */

use Task4\Employee as Employee4;

$obj4 = new Employee4('Павел', 500);

echo "<br><br>Задание 4<br>ЗП = " . $obj4->getSalary();
$obj4->doubleSalary();

echo "<br>ЗП = " . $obj4->getSalary();
$obj4->doubleSalary();
echo "<br>ЗП = " . $obj4->getSalary();

/**
 * Task 5
 */

use Task5\Rectangle as Rectangle5;

$obj5 = new Rectangle5(5, 10);
echo "<br><br>Задание 5<br>шырина 5, высота 10<br> площадь = " . $obj5->getSquare();
echo "<br>периметр прямоугольника = " . $obj5->getPerimeter();

/**
 * Task 6
 */

use Task6\User as User6;

echo "<br><br>Задание 6<br>li возраст 16 - 15 <br>";
$obj6 = new User6('li', 16);
$obj6->subAge(15);
echo "<br>новый возраст 90 + 40 <br>";
echo($obj6->setAge(90) . $obj6->addAge(40));

/**
 * Task 9
 */

use Task9\Employee as Employee9;

echo "<br><br>Задание 9<br>";
$obj9 = new Employee9();
$obj9->setName('Andrey');
$obj9->setAge(21);
$obj9->setSalary(10000);
echo $obj9->getName() . ' Получил ЗП в сумме ' . $obj9->getSalary();

/**
 * Task 11
 */

use Task11\Employee as Employee11;

echo "<br><br>Задание 11<br>";
$obj111 = new Employee11('Вася', 25, 1000);
$obj112 = new Employee11('Петя', 30, 2000);
echo 'сумма зарплат Васи и Пети ' . ($obj111->salary + $obj112->salary);

/**
 * Task 12
 */

use Task12\City as City12;

echo "<br><br>Задание 12<br>";
$obj12 = new City12('Kiev', 891, 8000000);
$obj12->getPropsValue();

/**
 * Task 13
 */

use Task13\Employee as Employee13;

echo "<br><br>Задание 13";
$obj13 = new Employee13();
$obj13->name = 'Иван';
$obj13->age = 25;
$obj13->salary = 1000;

$methods = ['method1' => 'getName', 'method2' => 'getAge'];
foreach ($methods AS $key => $value) {
    echo '<br>' . $obj13->$value();
}

/**
 * Task 14
 */

use Task14\User as User14;

echo "<br><br>Задание 14";
echo (new User14())->setName('Andrey')->setSurname('Anatoliyovich')->setPatronymic('Kravchenko')->getFullName();
echo (new User14())->setSurname('Anatoliyovich')->setPatronymic('Kravchenko')->setName('Andrey')->getFullName();

/**
 * Task 16
 */

use Task16\Employee as Employee16;
use Task16\Student as Student16;

$obj1611 = new Employee16('Ivan', 1500);
$obj1612 = new Employee16('Filip', 2500);
$obj1613 = new Employee16('Anton', 2100);
$obj1614 = new Student16('Frtem', 1100);
$obj1615 = new Student16('Egor', 900);
$obj1616 = new Student16('Aleksandr', 700);

// 16.3
$arr16 = array();
$arr16[] = $obj1611;
$arr16[] = $obj1614;
$arr16[] = $obj1612;
$arr16[] = $obj1616;
$arr16[] = $obj1613;
$arr16[] = $obj1615;

echo "<br><br>Задание 16";
//16.4
echo "<br><br>Работники:";
foreach ($arr16 AS $key161 => $value161) {
    if ($value161 instanceOf Employee16) {
        echo '<br>' . $value161->name;
    }
}

// 16.5
echo "<br><br>Студенты:";
foreach ($arr16 AS $key162 => $value162) {
    if ($value162 instanceOf Student16) {
        echo '<br>' . $value162->name;
    }
}

// 16.6
$sumSalary = 0;
$sumSholarship = 0;
foreach ($arr16 AS $key163 => $value163) {
    if ($value163 instanceOf Employee16) {
        $sumSalary += $value163->salary;
    }

    if ($value163 instanceOf Student16) {
        $sumSholarship += $value163->scholarship;
    }
}

echo "<br><br>Сумма зарплат: $sumSalary <br>сумма стипендий: $sumSholarship";

/**
 * Task 17
 */

// 17.1 - 17.3
use Task17\Employee as Employee17;
use Task17\User as User17;
use Task17\City as City17;

// 17.4
$obj1711 = new Employee17('Ivan', 'Kuzmuk', 1500);
$obj1712 = new Employee17('Filip', 'Kond', 2500);
$obj1713 = new Employee17('Andrew', 'Masters', 2100);

$obj1721 = new User17('Frtem', 'Borgom');
$obj1722 = new User17('Egor', 'Givchik');
$obj1723 = new User17('Aleksandr', 'Mfkbyjdcrbq');

$obj1731 = new City17('Kiev', 40000);
$obj1732 = new City17('Moskva', 80000);
$obj1733 = new City17('New Yerk', 50000);

$arr17 = array();
$arr17[] = $obj1711;
$arr17[] = $obj1721;
$arr17[] = $obj1731;
$arr17[] = $obj1712;
$arr17[] = $obj1722;
$arr17[] = $obj1732;
$arr17[] = $obj1733;
$arr17[] = $obj1723;
$arr17[] = $obj1713;

// 17.5
echo "<br><br>Задание 17";
echo "<br><br>Свойства name принадлежащие классу User или потомку этого класса:";
foreach ($arr17 AS $key171 => $value171) {
    if ($value171 instanceOf User17) {
        echo '<br>' . $value171->name;
    }
}

// 17.6
echo "<br><br>Свойства name НЕ принадлежащие классу User или потомку этого класса:";
foreach ($arr17 AS $key172 => $value172) {
    if (!$value172 instanceOf User17) {
        echo '<br>' . $value172->name;
    }
}

// 17.7
echo "<br><br>Свойства name принадлежащие только классу User:";
foreach ($arr17 AS $key173 => $value173) {
    if (get_class($value173) == 'Task_17\User') {
        echo '<br>' . $value173->name;
    }
}

/**
 * Task 18
 */

use Task18\Employee as Employee18;
use Task18\Post as Post18;

// 18.1 - 18.2
$obj181 = new Post18('программист', '1200');
$obj182 = new Post18('менеджер', '300');
$obj183 = new Post18('водитель', '890');
// 18.7
$obj184 = new Employee18('Andrey', 'Senichkin', $obj181);
// 18.8
echo "<br><br><b>!!!!!!!!!!!!!Задание 18</b><br>";
echo $obj184->getName();
echo $obj184->getSurname();
echo $obj184->getPostParams();

// 18.9
$obj184->changePost($obj181, 'спорцмен');
echo $obj184->getPostParams();

/**
 * Task 19
 */
/*
// task done
*/

/**
 * Task 20
 */

use Task20\StudentPartTime as StudentPartTime20;

$obj20 = new StudentPartTime20('Andrey', 4, 'I');
echo "<br><br>Задание 20<br>";
$obj20->isSurNameCorrect('I');
$obj20->isSurNameCorrect('Isfsdfsdfsdfsdf sdf sdf sdf sdf sdf sd asf af adf adf asdf asdf asf asdf asdf asdf asdf asdf adf asdf asdf adf asdf');

/**
 * Task 21
 */

use Task21\Employee as Employee21;

$obj21 = new Employee21('Andrey', 'Ivanov', '1983-12-10', 500);
echo "<br><br>Задание 21";
echo '<br>getName: ' . $obj21->getName();
echo '<br>getSurName: ' . $obj21->getSurName();
echo '<br>getBirthDay: ' . $obj21->getBirthDay();
echo '<br>getAge: ' . $obj21->getAge();
echo '<br>getSalary: ' . $obj21->getSalary();

/**
 * Task 22
 */

use Task22\ArraySumHelper as ArraySumHelper22;

echo "<br><br>Задание 22<br>";
echo ArraySumHelper22::getSum(array(2, 1, 6), 4);

/**
 * Task 23
 */

use Task23\Num as Num23;

echo "<br><br>Задание 23<br>";
echo '1 + 2 = ' . (Num23::$num1 + Num23::$num2);
echo '<br>sum = ' . Num23::getSum();
echo '<br>PI = ' . Num23::PI;
echo '<br>PR2 = ' . Num23::getConstant();

/**
 * Task 36
 */

/**
 * Task 37
 */