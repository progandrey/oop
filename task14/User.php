<?php

namespace Task14;

/**
 * Class User
 *
 * @package Task14
 *
 * @author Andrey <progandrey@gmail.com>
 *
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @example index.php
 *
 * @category Home Work
 *
 * @copyright 2019 The PHP course
 *
 * @version 1.0.0
 *
 * @param string $name
 * @param string $surname
 * @param string $patronymic
 */
class User
{
    private $name;
    private $surname;
    private $patronymic;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return object
     */
    public function setName(string $name): object
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return object
     */
    public function setSurname(string $surname): object
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     *
     * @return object
     */
    public function setPatronymic(string $patronymic): object
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * Get Full Name
     *
     * @return string
     */
    public function getFullName(): string
    {
        return substr($this->name, 0, 1) .
            substr($this->surname, 0, 1) .
            substr($this->patronymic, 0, 1);
    }
}